const N_TESTS_TO_RUN = 15000

describe("Junk specs", () => {
  Array(N_TESTS_TO_RUN)
    .fill()
    .map((_, index) => {
      const expected = index * 2;

      it(`should return ${expected}`, () => {
        expect(index * 2).toBe(expected);
      });
    });
});
